var express = require('express'),
    app = express(),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    users = [
        {name: 'alice', age: 20},
        {name: 'bob', age: 21},
        {name: 'charlie', age: 22}
    ];
app.listen(3000);
// serve static contents
// __dirname points to current directory
app.use(express.static(__dirname + '/public'));
// parse req/res body to json type
app.use(bodyParser.json());
// express middleware: a function that has access
// to req, res, next
// GET /hello
app.get('/hello', function(req, res, next) {
	if (Math.random() > 0.5) {
		res.send('Hello World');
		return;
	}
    res.send('Hello World');
});

app.get('/hello', function processNotAuthenticated(req, res, next) {
	res.send('<h2 style="color: red"> 2222</h2>');
});

app.get('/users', function getUsers(req, res, next){
	res.send(users);
});

app.post('/users', function postUsers(req, res, next) {
	var user = req.body;
	users.push(user);
	res.send(users);
});

app.get('/users/:name', function getUserByName(req, res) {
	var name = req.params.name,
		user = users.find(function findByName(u) {
			return name.toLowerCase() === u.name.toLowerCase();
		});
	res.send(user);
});

app.delete('/users/:name', function getUserByName(req, res) {
	var name = req.params.name,
		index = users.findIndex(function findIndex(u) {
			return u.name.toLowerCase === name.toLowerCase();
		});
		index !== -1 && users.splice(index, 1);
		res.send(users);
});

app.put('/users/:name', function getUsreBy(req, res) {
	var name = req.params.name,
		user = req.body;
		index = users.findIndex(function findIndex(u) {
			return u.name.toLowerCase === name.toLowerCase();
		});
	index === -1 ? users.push(user) : users.splice(index, 1, user);
	res.send(users);
});