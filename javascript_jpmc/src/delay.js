function delay(seconds, calculator, operation, nums) {
	return new Promise(function delayPromise(resolve, reject) {
		setTimeout(function(){
			if (calculator[operation] === undefined) {
				reject('rejected');
			} else {
				resolve(calculator[operation](nums[0], nums[1]));
			}
		}, seconds);
	});
}

module.exports = delay;