var Calculator = require('../src/Calculator');

function ScientificCalculator() {
	Calculator.call(this);
}

Calculator.prototype.sin = function (a, b) {
    return Math.sin(Math.PI / 2);
}

Calculator.prototype.cos = function (a, b) {
    return Math.cos(Math.PI);
}

Calculator.prototype.tan = function (a, b) {
    return Math.tan(0);
}

Calculator.prototype.log = function (a, b) {
    return Math.log(1);
}

ScientificCalculator.prototype = Object.create(Calculator.prototype);
module.exports = ScientificCalculator;

