var Calculator = require('../src/Calculator');

function withExponents(){
	this.pow = function(x, y) {
		return Math.pow(x, y);
	}

	this.multiplyExp = function(x, y) {
		return this.multiply(this.pow.apply(this, x), this.pow.apply(this, y));
	}

	this.divideExp = function(x,y) {
		return this.divide(this.pow.apply(this, x), this.pow.apply(this, y));
	}
}


module.exports = withExponents;