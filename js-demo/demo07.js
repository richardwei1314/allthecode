/* promise 
		an object that wraps an async operation that may succeed
		or fail.
	Aaron made a promise if his gf come to visit him,
	he will buy her a lipstick. 
	(Promise created, Pending)

	his gf is able to come, got her lipstick
	(Promise is resolved/fulfilled)

	his gf is not able to come
	(Promise is rejected)
*/

// callback function can do whatever promise can do
// callback hell, Promise is to avoid callback hell
/* sendRequest('http://someweb', function whatDay(res) {
	res.isSunday() &&
	sendRequest('...', function checkWeather(res) {
		res.isSunny() &&
		sendRequest('...', function isHealthy(res) {
			res.isHealthy() && ...
		})
	});
})*/

// ES6 API Promise
/*var myPromise = 
	new Promise(function testPromise(resolve, reject) {
		setTimeout(function simulateRequest() {
			var test = Math.random();
			if (test > 0.5) {
				resolve('resolved');
				// reject(); // does nothing
			} else {
				reject('rejected');
			}
		}, 2000);
	});
console.log(myPromise);
{
	state: 1,
	data: 'resolved',
	then: fn,
	catch: fn
	//err: 'rejected'
}

// how to handle promise
myPromise
	.then(function resolved(data) {
		console.log(data);
	})
	.catch(function rejected(err) {
		console.log(err);
	});

myPromise
	.then(
		function resolved(data) {
			console.log(data);
		},
		function rejected(err) {
			console.log(err);
		}
	);

*/
function createExam() {
	return new Promise(function createExamPromise(resolve, reject) {
		console.log('Creating exams:');
		setTimeout(function() {
			var res = Math.random() > 0.5;
			res ? resolve() : reject();
		}, 1000);
	});
}

function takeExam() {
	return new Promise(function takeExamPromise(resolve, reject) {
		console.log('taking the exam');
		setTimeout(function(){
			var score = Math.round(100 * Math.random());
			score > 50 ? resolve(score) : reject(score);
		}, 1000);
	});
}

createExam()
	.then(function createSuccess() {
		console.log('Exam is created successfully, now taking the exam...');
		return takeExam();
	}, function createFailed(){
		console.log('Create exam failed, exam is canceled');
		// no return means return;
		// return;
		/* if an error is thrown, the error object will be 
			wrapped in a rejected promise.
		   otherwise, the retrned result will be wrapped in
		   a resolved promise.
		   there is no way to jump out of promise chain	
		*/
		throw new Error();
	})
	.then(function passedExam(score) {
		console.log('Passed the exam, got', score);
	}, function failedExam(score) {
		!(score instanceof Error) &&
		console.log('failed the exam with', score);
	});