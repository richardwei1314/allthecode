/*
   ****** closure
   closure is talking about function inside of another function
   when outer function is done execution, the local variable
   of outer function being referenced in returned inner fn
   will be saved into closure(object).

   pros: avoid creating global(outer scope) variables
	cons: can easily cause memory leak
*/
function outer() {
	var x = 1,
		y = 2;
	function inner() {
		console.log(++x);
	}
	return inner;
}

var myInner = outer();

myInner(); // 1 from closure

// will there be another closure be created? Yes!
myInner = outer();
myInner();

// if return is non-primitive, o will get the returned object
// var o = new outer();

function counterFactory() {
	var count = 0;
	return {
		getCount: function getCount() {
			return count;
		},
		setCount: function setCount(c) {
			count = c;
		}
	}
}
var counter1 = counterFactory();
var counter2 = counterFactory();
counter1.setCount(5);
console.log(counter1.getCount(), counter2.getCount());


/* Interview Question */
function increaseFactory() {
	// your code goes here...
	var c = 0;
	return function() {
		console.log(++c);
	};
}

var myIncrease = increaseFactory();
myIncrease(); myIncrease();  // 1 2


// spoiler
function MyClass() {
	this.x = 100; // {x: 100}
	var y = 'abc';
	var getX = function() {
		console.log(this.x); // undefined
		console.log(y);      // abc
	};
	getX();
	this.getY = function() {
		console.log(this.x); // 100
		console.log(y);		 // abc (from closure)
	};
}
var myClass = new MyClass();
// {x: 100, getY: fn}
myClass.getY();
// output: undefined, abc, 100, abc