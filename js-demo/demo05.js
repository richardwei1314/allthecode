// **** protype
function A() {
	this.value = 10;
	this.getV = function getV() {
		return this.value;
	};
}

var a1 = new A(),
	a2 = new A();

console.log(a1.getV === a2.getV);

// ------- prototype ----------
console.log('---------------- prototype ---------------');

function B() {
	this.value = 20;
}
B.prototype.getV = function() {
	return this.value;
};

var b1 = new B(),
	b2 = new B();
console.log(b1, b1.getV(), b2.getV());
console.log(b1.getV === b2.getV);


function Parent() {
	this.x = 10;
}
Parent.prototype.y = 20;

function Child() {
	this.z = 30;
}
var p = new Parent();
Child.prototype = p;

var c = new Child();

console.log(c.x, c.y, c.z); // 10 20 30
console.log(p.constructor, Parent.prototype.constructor);
console.log(c.constructor);


/*
	var data =[{name: 'bob', age: 12},{name: 'alice', age: NaN}, ...]
	function getTotalAge(arr) {
		return arr
				.filter()
				.reduce()
	}
	getTotalAge(data);

*/

function Shape() {
	this.p_type = 'shape';
}
Shape.prototype.getType = function() {
	return this.p_type;
};

function Circle() {
	// simulate super()
	Shape.call(this);
	this.c_type = 'circle';
}

Circle.prototype = Object.create(Shape.prototype);
// constructor reset
Circle.prototype.constructor = Circle;

var circle = new Circle();
// shape, shape, circle
console.log(circle.p_type, circle.getType(), circle.c_type);
console.log(circle.constructor); // Circle

// how to check if a prototype is on an object's prototype chain
console.log(Object.prototype.isPrototypeOf(circle));
// instanceof: isA relationship
console.log(circle instanceof Shape);
