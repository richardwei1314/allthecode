var order = {
	price: 100,
	calculate: function calculate() {
		return {
			getTotal: function getTotal(qty) {
				return qty * this.price;
			}
		};
	}
};

var obj = order.calculate();

console.log(obj.getTotal.call(order, 5));

var myOrder = order.calculate().getTotal;
var res = myOrder.apply({price: 100}, [5]);
console.log(res);

