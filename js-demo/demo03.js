/* this keyword in js
	in constructor function, this binds to the newly created obj
	in non-constructor function, 
		if there is a caller, this binds to the caller,
		if no caller, this binds to global

 */
function Vehicle(brand) {
	this.brand = brand;
}

var v = new Vehicle('Huge Light');
console.log(v.brand);

var u = Vehicle('Red Flag');
console.log(u);
/* 
When accessing a variable, js will look for the variable locally
first, if not found, it will continute to go to the upper/outer
scope until global, if not found in global, js will throw an error

When doing assignment, js will look for the variable locally
first, if not found, it will continute to go to the upper/outer
scope until global, if not found in global, it will create that
property under global
*/
console.log(brand);

var bob = {
	name: 'bob',
	hi: function hi(prefix, suffix) {
		console.log('hi', prefix, this.name, suffix);
	}
};
bob.hi();

var myHi = bob.hi;
myHi();
var charlie = {
	name: 'charlie',
	hi: bob.hi
};
charlie.hi();

// how to change binding of this?
// 1. call   2.apply   3.bind

// 1st param: new binding for this keyword
myHi.call(bob);

myHi.apply({name: 'Alice'});

// return a new function with binding of this changed.
myHi.bind(charlie)();


myHi.call(bob, 'Mr.', 'Smart');
myHi.apply(bob, ['Mr.', 'Junior']);
myHi.apply(bob, {0: 'Super', 1: 'Dumb', length: 2});

var newMyhi = myHi.bind(charlie,'Grand');

newMyhi('Magic');



var order = {
	price: 100,
	calculate: function calculate() {
		return {
			getTotal: function getTotal(qty) {
				return qty * this.price;
			}
		};
	}
};

// call getTotal(5)  should return 500
console.log(
	order.calculate().getTotal.apply(order, [5])
);


// code review
// SOLID principle
function checkout(price, membership) {
	switch(membership) {
		case 'Bronze':
			return price * 0.9;
		case 'Silver':
			return price * 0.8;
		case 'Gold':
			return price * 0.7;
		// case 'VIP'
		default:
			return price;
	}
}
console.log(checkout(500, 'Silver'));
console.log(checkout(500, 'VIP'));

var members = {
	Bronze: 0.9,
	Silver: 0.8,
	Gold: 0.7,
	VIP: 0.6
};
function checkout2(price, membership) {
	return price * (members[membership] || 1);
}

console.log(checkout2(500, 'ABC'));
console.log(checkout2(500, 'VIP'));



var members2 = {
	Bronze: 0.8,
	Silver: 0.7,
	Gold: 0.6,
	VIP: 0.5
};
// ultimate solution
function checkout3(price, membership) {
	return price * (this[membership] || 1);
}
console.log(checkout3.call(members, 500, 'VIP'));
console.log(checkout3.call(members2, 500, 'VIP'));