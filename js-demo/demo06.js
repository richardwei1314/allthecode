// js runtime is single threaded
/*
	simulate multi-threading:
	browser: HTML5 Web Worker
	NodeJS: Child Process, Cluster module
	
	Event loop is talking about how js handle
	 sync and async operations
	For ASYNCHRONOUS scenario:
	Whenever seeing asynchronous operation such as setTimeout,
	it will be moved to Web APIs (pool),
	Once the function is timed out in Web APIs,
	it will be moved to Queue. JS continue
	execution of rest of synchronous codes. When Stack is empty,
	js starts to move functions from Queue to Stack for execution.
	**** All the async functions will be
	executed after sync functions/operations.
*/
// callback function: create a function and gets called later
var myTimeout = setTimeout(function delay() {
	console.log('timed out');
}, 2000);
console.log('outside of timeout');



var myInterval = setInterval(function interval() {
	console.log(new Date());
}, 1000);

setTimeout(function stop() {
	clearTimeout(myTimeout);
	clearInterval(myInterval);
}, 1500);


// how to use setTimeout to implement setInterval
function customInterval() {

}

customInterval(function interval() {
	console.log(new Date());
}, 1000);